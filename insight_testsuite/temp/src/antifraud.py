'''
 * Name            : antifraud.py
 * Project         : Digital-Wallet
 * Description     : Implementation of features to prevent fraudulent payment requests from untrusted users.
 * Copyright       : 2016 YU-JHE LI. All rights reserved.
 * Creation Date   : 11/6/16
 * Original Author : YU-JHE LI
'''
#include library
from copy import copy, deepcopy
import sys
from datetime import datetime
#Set the most number of id. default 90000. Can be change if needed.
Number=90000

# Open the files from arguments.
f1 = open(sys.argv[1], "r")
f2 = open(sys.argv[2], "r")
f3 = open(sys.argv[3], "w")
f4 = open(sys.argv[4], "w")
f5 = open(sys.argv[5], "w")
#f6 = open("debug.txt", "w")

# Create a new relationship dictionary array for most 90000 users
mat= [dict() for _ in range (Number)] 
#new_mat=[dict() for _ in range (Number)] 

# used to neglect first line of the input file
firstline_flag=0

#document the time now
now = datetime.now() 
print '[%s/%s/%s %s:%s:%s]' % (now.month, now.day, now.year, now.hour, now.minute, now.second)
print "Start reading data from batch_payment.txt"
tag=0
thethe=0
# Start reading data from batch_payment.csv
for line in f1.readlines():
    if firstline_flag>0:
        # Strip the line to remove whitespace.
        line = line.strip()
        # Split the line.
        parts = line.split(", ")
        #use length to remove long message line for few cases
        if len(parts)>=5:
            id1=int(parts[1])
            id2=int(parts[2])
            #set the relationship into memory
            mat[id1][id2]=1
            mat[id2][id1]=1
            
    else:
        firstline_flag+=1
#document the time now      
now = datetime.now()
print '[%s/%s/%s %s:%s:%s]' % (now.month, now.day, now.year, now.hour, now.minute, now.second)
print "read batch_payment.txt successful"        

#document the time now
now = datetime.now()
print '[%s/%s/%s %s:%s:%s]' % (now.month, now.day, now.year, now.hour, now.minute, now.second)
print "Starting handling paths for each modes using BFS with degree-2" 

# Starting distance handling from memory to build degree-2 network

new_mat=deepcopy(mat)
for i in xrange(Number):

    '''#For debugging   
    if i%10000==0:
        print "Around "+str(i)+" nodes in the Network is handled"
    ''' 
    for key in mat[i] :
        for key2 in mat[key]:
            if key2 in mat[i]:
                new_mat[i][key2]=min(mat[i][key2],mat[i][key]+mat[key][key2])               
            else:   
                new_mat[i][key2]=mat[i][key]+mat[key][key2]
                

# used to neglect first line of the input file
firstline_flag=0;
#flag used to detect degree between 3 and 4
flag_more=0

#document the time now  
now = datetime.now()
print '[%s/%s/%s %s:%s:%s]' % (now.month, now.day, now.year, now.hour, now.minute, now.second)
print "Start dealing with the work at stream_payment.txt and write out to files"      

#Start dealing with the work at stream_payment.csv      
for line in f2.readlines():
    if firstline_flag>0:
        # Strip the line to remove whitespace.
        line = line.strip()
        # Split the line.
        parts = line.split(", ")
        #use length to remove long message line for few cases
        if len(parts)>=5:
            time=parts[0]
            
            # catch the id1 and id2 
            id1=int(parts[1])
            id2=int(parts[2])
            #f6.write(str(id1)+', '+str(id2)+'\n')
            
            # the transaction amout 
            amount=float(parts[2])
            # reset flag
            flag_more=0
            flag_update=0       
            if id2 in new_mat[id1]:
                distance=new_mat[id1][id2]
                if distance==1:
                    f3.write('trusted\n')
                    f4.write('trusted\n')
                    f5.write('trusted\n')
                    #f6.write(str(id1)+', '+str(id2)+'\n')
                    
                else:
                    f3.write('unverified\n')
                    f4.write('trusted\n')
                    f5.write('trusted\n')
                    #f6.write(str(id1)+', '+str(id2)+'\n')
                    flag_update=1
            
            else:
                for key in new_mat[id1]:
                    if key in new_mat[id2]:
                        f3.write('unverified\n')
                        f4.write('unverified\n')
                        f5.write('trusted\n')
                        #f6.write(str(id1)+', '+str(id2)+'\n')
                        flag_more=1
                        flag_update=1
                        break
                if flag_more==0: 
                    flag_update=1
                    f3.write('unverified\n')
                    f4.write('unverified\n')
                    f5.write('unverified\n')
                    #f6.write(str(id1)+', '+str(id2)+'\n')
            
            #update the network        
            if flag_update==1:                 
                mat[id1][id2]=1
                mat[id2][id1]=1
                new_mat[id1][id2]=1
                new_mat[id2][id1]=1
                for key in mat[id2]:
                    if key in mat[id1]:
                        new_mat[id1][key]=min(mat[id1][key],mat[id1][id2]+mat[id2][key])  
                        new_mat[key][id1]= new_mat[id1][key]         
                    else:   
                        new_mat[id1][key]=mat[id1][id2]+mat[id2][key]   
                        new_mat[key][id1]= new_mat[id1][key] 
                        
                for key in mat[id1]:
                    if key in mat[id2]:
                        new_mat[id2][key]=min(mat[id2][key],mat[id2][id1]+mat[id1][key])  
                        new_mat[key][id2]= new_mat[id2][key] 
                    else:   
                        new_mat[id2][key]=mat[id2][id1]+mat[id1][key]
                        new_mat[key][id2]= new_mat[id2][key]
                
    else:
        firstline_flag+=1
        
#document the time now          
now = datetime.now()
print '[%s/%s/%s %s:%s:%s]' % (now.month, now.day, now.year, now.hour, now.minute, now.second)
print "Work done"  
f1 .close()
f2 .close()
f3 .close()
f4 .close()
f5 .close()
#f6 .close()
   