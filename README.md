# Design Justifications for Digital-wallet

#### Developer:
- Yu-Jhe Li

#### Instructions 
> Imagine you're a data engineer at a "digital wallet" company called PayMo that allows users to easily request and make payments to other PayMo users. The team at PayMo has decided they want to implement features to prevent fraudulent payment requests from untrusted users.


### 1 Design Description

Before I designed the project to achieve all features mentioned below, I constructed user graph where users can find their friends in the neibor connected nodes. To save the memory and avoid using adjacency matrix for finding shortest paths to every nodes in the graph. I tried to utilize "map" for storing the only close neighbor nodes for each node. By this method, I can modified the distance limit in the map to attain the needs rather than store all distances of paths toward every nodes.              

I used Breath-first-search algorithm for every user nodes to generate the their own network. Set the proper limit of distance using BFS, and thus network can be used for different order degree relationship. For example, if two users have close relationship, both their own newwork will overlap in more than one nodes. Sometimes if the distance of BFS is low, it is not likely both networks will overlap. Setting high distance can make network bigger but cost more unused memory.

###Feature 1
> When anyone makes a payment to another user, they'll be notified if they've never made a transaction with that user before.

For this feature, I just set the distance of BFS in each of user's map to `1`. Thus, the network will "only" have neighbor nodes who is 1 far from the user. If one user is in another user's network, this idicates that they are friends.


###Feature 2
> The PayMo team is concerned that these warnings could be annoying because there are many users who haven't had transactions, but are still in similar social networks. 
>For example, User A has never had a transaction with User B, but both User A and User B have made transactions with User C, so User B is considered a "friend of a friend" for User A.
>For this reason, User A and User B should be able to pay each other without triggering a warning notification since they're "2nd degree" friends. 

<img src="./images/friend-of-a-friend1.png" width="500">

To design this, I set the the distance of BFS in each of user's map to `2`. Thus, the network will "only" have neighbor nodes who is 1 far from the user. If one user is in another user's network, this idicates that they are "friends" or "friend of a friend".

###Feature 3
>More generally, PayMo would like to extend this feature to larger social networks. Implement a feature to warn users only when they're outside the "4th degree friends network".

<img src="./images/fourth-degree-friends2.png" width="600">

In the above diagram, payments have transpired between User

* A and B 
* B and C 
* C and D 
* D and E 
* E and F

To design for this, initially I tried to set the distance to "4" but only to see the program waste a great deal of time. However, there is another method is that we can detect whether two users' "2nd degree" nework have same neighbors. If they do, they may be "4th degree friends" or "3th degree friends", which are qualified for the features.

###Feature 4
 I think about sometimes the amount of money for transaction is also a important reference, thus I set a threshold and examinine whether the amount exceed the threshold. If it does, apply feature 2 condition, otherwise apply feature 3 condition.
 
##Details of implementation

### 1 Programming language and used Data type

I used python for the project after comparing the flexibility with java or C++. More detailed code like read and write are in the `antifraud.py`. Here just mentioned important concept of my project.

To implement the "map" and BFS mentioned above, I used the dictionary type, which is also kind of map in python, to store the neighbors and corresponding distance. The `number` below means the number of users.

	mat= [dict() for _ in range (Number)] 
	
To create "2nd order" from "first order" networks, just used loop to detect and add their neighbor's neighbor in their network.

	for i in xrange(Number):
		for key in mat[i] :
			for key2 in mat[key]:
				if key2 in mat[i]:
					new_mat[i][key2]=min(mat[i][key2],mat[i][key]+mat[key][key2])               
				else:   
					new_mat[i][key2]=mat[i][key]+mat[key][key2]
                  
To decide either to write `trusted` or `unverified` based on different conditions:

- For the first feature, used only first degree networks and detected the existence of the user to be paid. By this to dicide giving `trusted` or `unverified`.

- For the feature two, just used similar way like second-degree networks.

- For the feature three, decide either to write `trusted` or `unverified` based on detecting thether their second-degree networks overlap. If they do, they must be "4th degree friends" or "3th degree friends and should be given `trusted`, otherwise `unverified`.

- For the feature four I made, just use `if` statement to decide whether it should be feature 2 or feature 3   

The input files are in the `paymo_input` directory, which are `batch_payment.txt` and `stream_payment.txt`.
The output files are in the `paymo_output` directory, which are `output1.txt`, `output2.txt` and `output3.txt`.

For more details, look at `antifraud.py` or email me at jackli16398@gmail.com if you have more questions about the project.

